import 'dart:io';
import 'package:flutter/material.dart';


class ProfileDeveloper extends StatefulWidget {
  const ProfileDeveloper({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ProfileDeveloper> createState() => _ProfileDeveloperState();
}

class _ProfileDeveloperState extends State<ProfileDeveloper> {



  @override


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('System Developer'),

      ),
      body: Container(

        child: Padding(

          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              ListTile(

                leading:CircleAvatar(
                  backgroundImage: AssetImage("asset/images/u.png",),
                ),
                title: const Text('นางสาวณภัสสรณ์ การเรียบ'),
                subtitle: Text('6350110003@psu.ac.th'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Divider(
                thickness: 0.8,
              ),
              ListTile(

                leading:CircleAvatar(
                  backgroundImage: AssetImage("asset/images/u.png",),
                ),
                title: const Text('นางสาว ณัสมา สุภาวรรณ์'),
                subtitle: const Text('6350110006@psu.ac.th'),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Divider(
                thickness: 0.8,
              ),
            ],

          ),
        ),

      ),


    );
  }
}
