import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreatePro extends StatefulWidget {

  @override
  _CreateProState createState() => _CreateProState();
}

class _CreateProState extends State<CreatePro> {
  // Handles text

  final nameController = TextEditingController();
  final priceController = TextEditingController();
  final descriptionController = TextEditingController();

  final GlobalKey<FormState> _key=new GlobalKey<FormState>();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Insert Product"),
        backgroundColor: Colors.black,
      ),


      body: Container(
        height: 970.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom:1 ),
              child: Image.asset('asset/images/12.png',),
            ),


            Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Create Product now!',style:TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    SizedBox(height: 3,),
                    TextFormField(
                      controller: nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products name',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10,),

                    TextFormField(
                      controller: priceController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products price',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter price';
                        }

                        return null;
                      },
                      keyboardType: TextInputType.number,
                    ),
                    SizedBox(height: 10,),
                    TextFormField(
                      controller: descriptionController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'description',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter description';
                        }
                        return null;
                      },
                    ),

                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.black)
                      ),
                        onPressed: (){
                      if(_key.currentState!.validate()) {
                        _onConfirm(context);
                      }
                    },child:Text('Confirm',))
                  ],

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
