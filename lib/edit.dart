import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Products.dart';

class Edit extends StatefulWidget {
  final Products products;

  Edit({required this.products});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final GlobalKey<FormState> _key=new GlobalKey<FormState>();

  // Http post request
  Future editProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.products.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text

      },
    );
  }

  void _onConfirm(context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.products.name);
    priceController = TextEditingController(text: widget.products.price.toString());
    descriptionController = TextEditingController(text: widget.products.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Products"),
        backgroundColor: Colors.black,
      ),
      body: Container(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[



            Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20,),
                    Text('Edit your product',style:TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    SizedBox(height: 3,),
                    TextFormField(
                      controller: nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products name',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10,),

                    TextFormField(
                      controller: priceController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter products price',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter price';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                    ),
                    SizedBox(height: 10,),
                    TextFormField(
                      controller: descriptionController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'description',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter description';
                        }
                        return null;
                      },
                    ),

                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.black)
                        ),
                        onPressed: (){
                          if(_key.currentState!.validate()) {
                            _onConfirm(context);
                          }
                        },child:Text('Confirm',))
                  ],

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
